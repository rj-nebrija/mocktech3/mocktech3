let collection = [];


// Write the queue functions below.


// 1. Output all the elements of the queue
const print = () => {
	return collection
}


// 2. Adds element to the rear of the queue
const enqueue = (element) => {
	collection = [...collection, element]
	return collection
}



// 3. Removes element from the front of the queue
const dequeue = () => {
	let newCollection = []
	let size = 0
	for(i = 1; i < collection.length; i++ ) {
		newCollection[size] = collection[i]
		size += 1
	}

	collection = [...newCollection]
	return collection
}


// 4. Show element at the front
const front = () => {
	return collection[0]
}

// 5. Show the total number of elements
const size = () => {
	return collection.length
}

// 6. Outputs a Boolean value describing whether queue is empty or not
const isEmpty = () => {
	return (collection.length == 0)
}


module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};